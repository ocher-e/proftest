package by.test.its.controller.rest;

import by.test.its.model.User;
import by.test.its.service.interfaces.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserRESTController {
    @Autowired
    UserService userService;
    
    @GetMapping("/users")
    public List<User> getUsers() {
        return userService.findAll();
    }
    
    @GetMapping("/users/{id}")
    public ResponseEntity getUser(@PathVariable("id") Long id) {
        User user = userService.get(id);
        if(user == null) {
            return new ResponseEntity("There is no user for Id " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(user, HttpStatus.OK);
    }
    
    @PostMapping("/users")
    public ResponseEntity createUser(@RequestBody User user) {
        userService.saveOrUpdate(user);
        return new ResponseEntity(user, HttpStatus.OK);
    }
    
    @PutMapping("/users/{id}")
    public ResponseEntity updateUser(@PathVariable("id") Long id, @RequestBody User user) {
        User curUser = userService.get(id);

            if (null == curUser) {
                    return new ResponseEntity("No user found for Id " + id, HttpStatus.NOT_FOUND);
            }
            
            userService.saveOrUpdate(user);
            return new ResponseEntity(user, HttpStatus.OK);
    }
    
    @DeleteMapping("/users/{id}")
    public ResponseEntity deleteUser(@PathVariable Long id) {
            if (null == userService.get(id)) {
                    return new ResponseEntity("No user found for Id " + id, HttpStatus.NOT_FOUND);
            }
            userService.delete(id);
            return new ResponseEntity(id, HttpStatus.OK);
    }
}
