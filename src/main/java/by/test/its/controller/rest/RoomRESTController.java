package by.test.its.controller.rest;

import by.test.its.model.Room;
import by.test.its.service.interfaces.RoomService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoomRESTController {
    @Autowired
    RoomService roomService;
    
    @GetMapping("/rooms")
    public List<Room> getRooms() {
        return roomService.findAll();
    }
    
    @GetMapping("/rooms/{id}")
    public ResponseEntity getRoom(@PathVariable("id") Long id) {
        Room room = roomService.get(id);
        if(room == null) {
            return new ResponseEntity("There is no room for Id " + id, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(room, HttpStatus.OK);
    }
    
    @PostMapping("/rooms")
    public ResponseEntity createRoom(@RequestBody Room room) {
        roomService.saveOrUpdate(room);
        return new ResponseEntity(room, HttpStatus.OK);
    }
    
    @PutMapping("/rooms/{id}")
    public ResponseEntity updateRoom(@PathVariable("id") Long id, @RequestBody Room room) {
        Room curRoom = roomService.get(id);

            if (null == curRoom) {
                    return new ResponseEntity("No room found for Id " + id, HttpStatus.NOT_FOUND);
            }
            
            roomService.saveOrUpdate(room);
            return new ResponseEntity(room, HttpStatus.OK);
    }
    
    @DeleteMapping("/rooms/{id}")
    public ResponseEntity deleteRoom(@PathVariable Long id) {
            if (null == roomService.get(id)) {
                    return new ResponseEntity("No room found for Id " + id, HttpStatus.NOT_FOUND);
            }
            roomService.delete(id);
            return new ResponseEntity(id, HttpStatus.OK);
    }

}
