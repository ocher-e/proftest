package by.test.its.controller;

import by.test.its.model.User;
import by.test.its.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
    @Autowired
    UserService userService;
    
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String welcomNewUser(ModelMap model) {
        User currentUser = userService.saveOrUpdate(new User());
        model.addAttribute("currentUserId", currentUser.getUserid());
        return "home";
    }
    
    @RequestMapping(value = "/exit/{userId}", method = RequestMethod.GET)
    public String exitTheFlat(@PathVariable Long userId, ModelMap model) {
        userService.delete(userId);
        return "bye";
    }
}
