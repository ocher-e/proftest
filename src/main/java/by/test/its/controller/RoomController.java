package by.test.its.controller;

import by.test.its.model.Room;
import by.test.its.service.interfaces.RoomService;
import by.test.its.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import by.test.its.service.interfaces.LogicService;

@Controller
public class RoomController {
    @Autowired
    RoomService roomService;
    
    @Autowired
    UserService userService;
    
    @Autowired
    LogicService logicService;
    
    @RequestMapping(value = "/room/{roomId}/{userId}", method = RequestMethod.GET)
    public String comeInTheRoom(@PathVariable Long roomId,
                                @PathVariable Long userId, ModelMap model) {
        logicService.comeInRoom(userService.get(userId), roomService.get(roomId));
        return "redirect:/showRoom/{roomId}/{userId}";
    }
    
    @RequestMapping(value = "/out/{roomId}/{userId}", method = RequestMethod.GET)
    public String goOutOfTheRoom(@PathVariable Long roomId,
                                 @PathVariable Long userId, ModelMap model) {
        model.addAttribute("currentUserId", userId);
        logicService.goOutOfRoom(userService.get(userId), roomService.get(roomId));
        return "home";
    }
    
    @RequestMapping(value = "/changeLampState/{roomId}/{userId}", method = RequestMethod.GET)
    public String changeLampState(@PathVariable Long roomId,
                                  @PathVariable Long userId, ModelMap model) {
        logicService.changeLampStateOfRoom(roomService.get(roomId));
        return "redirect:/showRoom/{roomId}/{userId}";
    }
    
    @RequestMapping(value = "/showRoom/{roomId}/{userId}", method = RequestMethod.GET)
    public String showRoom(@PathVariable Long roomId,
                           @PathVariable Long userId, ModelMap model) {
        model.addAttribute("currentUserId", userId);
        Room currentRoom = roomService.get(roomId);
        model.addAttribute("currentRoom", currentRoom);
        return "room";
    }
}
