package by.test.its.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="T_USER")
public class User implements Serializable, Observer {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "F_USER_ID", unique = true, nullable = false)
    private Long userid;
    
    @Column(name = "F_LAMP_STATE")
    private boolean lampstate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "F_ROOM_ID", nullable = true)
    @JsonBackReference
    private Room room;

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }    
    
    public boolean getLampstate() {
        return lampstate;
    }

    public void setLampstate(boolean lampstate) {
        this.lampstate = lampstate;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public void update() {
        this.lampstate = room.getLampstate();
    }

}
