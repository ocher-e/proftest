package by.test.its.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "T_ROOM")
public class Room implements Serializable, Subject {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "F_ROOM_ID", unique = true, nullable = false)
    private Long roomid;
    
    @Column(name = "F_LAMP_STATE")
    private boolean lampstate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "room")
    @JsonManagedReference
    private Set<User> users = new HashSet<>(0);  
    
    public Room() {}
    
    public Room(boolean lampstate) {
        this.lampstate = lampstate;
    }

    public Long getRoomid() {
        return roomid;
    }

    public void setRoomid(Long roomid) {
        this.roomid = roomid;
    }

    public boolean getLampstate() {
        return lampstate;
    }

    public void setLampstate(boolean lampstate) {
        this.lampstate = lampstate;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public void attach(Observer o) {
        User u = (User)o;
        u.setLampstate(this.getLampstate());
        u.setRoom(this); 
        users.add(u);
    }

    @Override
    public void detach(Observer o) {
        User u = (User) o;
        users.remove(u);
        u.setLampstate(false); // сброс значения в начальное
        u.setRoom(null);
    }

    @Override
    public void notifyObservers() {
        for(User u : users) {
            u.update();
        }
    }
    
}
