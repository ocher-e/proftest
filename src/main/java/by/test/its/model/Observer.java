package by.test.its.model;

public interface Observer {
    public void update();
}
