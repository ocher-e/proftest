package by.test.its.service.interfaces;

import by.test.its.model.Room;
import java.util.List;

public interface RoomService {
    public Room saveOrUpdate(Room r);
    public Room get(Long id);
    public List<Room> findAll();
    public void delete(Long id);
}
