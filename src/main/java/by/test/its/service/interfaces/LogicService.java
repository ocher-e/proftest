package by.test.its.service.interfaces;

import by.test.its.model.Room;
import by.test.its.model.User;

public interface LogicService {
    public void comeInRoom(User user, Room room);
    public void goOutOfRoom(User user, Room room);
    public void changeLampStateOfRoom(Room room);
}
