package by.test.its.service.interfaces;

import by.test.its.model.User;
import java.util.List;

public interface UserService {
    public User saveOrUpdate(User u);
    public User get(Long id);
    public void delete(Long id);
    public List<User> findAll();
}
