package by.test.its.service.impl;

import by.test.its.model.User;
import by.test.its.repository.UserRepository;
import by.test.its.service.interfaces.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository repository;

    @Override
    @Transactional
    public User saveOrUpdate(User u) {
        return repository.save(u);
    }

    @Override
    @Transactional(readOnly = true)
    public User get(Long id) {
        return repository.findOne(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return (List<User>) repository.findAll();
    }
    
}
