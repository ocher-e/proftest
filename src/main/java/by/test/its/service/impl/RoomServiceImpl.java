package by.test.its.service.impl;

import by.test.its.model.Room;
import by.test.its.repository.RoomRepository;
import by.test.its.service.interfaces.RoomService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoomServiceImpl implements RoomService {
    @Autowired
    RoomRepository repository;

    @Override
    @Transactional
    public Room saveOrUpdate(Room r) {
        return repository.save(r);
    }

    @Override
    @Transactional(readOnly = true)
    public Room get(Long id) {
        return repository.findOne(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Room> findAll() {
        return (List<Room>) repository.findAll();
    }

    @Override
    @Transactional
    public void delete(Long id) {
        repository.delete(id);
    }
}
