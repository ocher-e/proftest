package by.test.its.service.impl;

import by.test.its.model.Room;
import by.test.its.model.User;
import by.test.its.service.interfaces.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import by.test.its.service.interfaces.LogicService;

@Service
public class LogicServiceImpl implements LogicService {
    @Autowired
    RoomService roomService;

    @Override
    @Transactional
    public void comeInRoom(User user, Room room) {
        room.attach(user);
        roomService.saveOrUpdate(room);
    }

    @Override
    @Transactional
    public void goOutOfRoom(User user, Room room) {
        room.detach(user);
        roomService.saveOrUpdate(room);
    }

    @Override
    @Transactional
    public void changeLampStateOfRoom(Room room) {
        room.setLampstate(!room.getLampstate());
        room.notifyObservers();
        roomService.saveOrUpdate(room);
    }
}
