<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Room</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <% response.setIntHeader("Refresh", 5); %>
        <h2>Room ${currentRoom.roomid}</h2>
        <h3>Your id is ${currentUserId}</h3>
        <h3>Lamp state ${currentRoom.lampstate}</h3>
        <ul>
            <li><a href ="<c:url value="/changeLampState/${currentRoom.roomid}/${currentUserId}"/>">Change the lamp status</a></li>
            <li><a href="<c:url value="/out/${currentRoom.roomid}/${currentUserId}"/>">Go out of the room</a></li>
        </ul>
    </body>
</html>


