<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flat</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h2>Hi! Your id is ${currentUserId}</h2>
        <h2>Choose the room, please</h2>
        <ul>
            <li><a href="<c:url value="/room/1/${currentUserId}" />">room 1</a></li>
            <li><a href="<c:url value="/room/2/${currentUserId}" />">room 2</a></li>
            <li><a href="<c:url value="/room/3/${currentUserId}" />">room 3</a></li>
            <li><a href="<c:url value="/exit/${currentUserId}" />">Exit the flat</a></li>
        </ul>
    </body>
</html>

